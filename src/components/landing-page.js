import React from 'react';
import { fontWeight, styled } from '@mui/system';
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  Container,
  Box,
  CssBaseline,
} from '@mui/material';
import logo from './public/logo.png';
import firstsection1 from './public/firstsection1.png';
import parser from './public/itparser.png'

const AppContainer = styled('div')({
    backgroundColor: 'whitesmoke', // Set the background color for the entire page
  });

const Header = styled(AppBar)(({ theme }) => ({
  backgroundColor: 'whitesmoke',
  padding: '10px 0 10px 0',
  color: 'black',
  fontWeight: 500,
  fontSize: 17,
  letterSpacing: 0.05,
  height: '100px',
  boxShadow: 'none', // Remove box shadow
}));

const HeaderInner = styled(Toolbar)(({ theme }) => ({
  margin: '0 55px',
  marginTop: '20px',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  '& a:first-child': {
    marginLeft: 10,
    marginTop: 10,
    textDecoration: 'none',
  },
  '& nav a': {
    textDecoration: 'none',
    color: 'black',
    marginLeft: 45,
    display: 'inline-block',
    fontWeight: 'bold',
    fontSize: '20px',

  },
  '& a:last-child': {
    backgroundColor: '#800000',
    borderRadius: 13,
  },

  '& .learnMore': {
    backgroundColor: '#800000', // Change the background color to maroon for the SIGN IN button
    borderRadius: 13,
    '&:hover': {
      backgroundColor: '#800000', // Keep the same color on hover for the SIGN IN button
      borderRadius: 13,
      color: 'black',
    },
},

'& .logo-container': {
    display: 'flex',
    alignItems: 'center',
  },

'& .logo-text': {
    marginLeft: '10px',
    fontSize: '24px',
    fontWeight: 'bold',
    color: 'black',
    display: 'flex',
    textDecoration: 'none',
  },

}));

const MainContainer = styled(Container)(({ theme }) => ({
  margin: '0 95px',
  position: 'relative',
  marginTop: '20px',
  zIndex: 1,
}));

const FirstSection = styled('section')(({ theme }) => ({
  position: 'fixed',
  backgroundColor: 'whitesmoke',
  padding: '60px 10px 43px 100px',
  marginTop: '100px',
  height: '805px',
  right: 0,
  left: 0,
  bottom: 0,
  top: 0,
  '& h1': {
    color: 'maroon',
    fontSize: 86,
    margin: '25px 25px 25px 1px',
    fontWeight: 'bold',
  },
  '& p': {
    color: '#f0b1b5',
    fontSize: 23,
    fontFamily: 'MafraBold',
    lineHeight: 34,
    fontWeight: 'bold',
    margin: '30px 15px',
  },
  '& > div': {
    display: 'flex',
  },
}));

const ButtonStyled = styled(Button)(({ theme }) => ({
  cursor: 'pointer',
  backgroundColor: 'maroon',
  padding: '10px 65px',
  color: 'white',
  fontSize: 18,
  fontWeight: 'bold',
  borderRadius: 13,
  border: 'none',

}));

const FirstSectionIcon = styled('img')(({ theme }) => ({
  height: 86,
  position: 'absolute',
  left: 0,
  top: '86%',
}));

const Logo = styled('img')(({ theme }) => ({
  width: 'auto',
  height: 35,
}));



function LandingPage() {
  return (
    <div>
      <CssBaseline />
      <Header>
        <HeaderInner>
        <a href="a#" className="logo-container">
            <Logo src={parser} sx={{color: 'red'}} alt="Prodigitas Logo" />
            <Typography className="logo-text">IT DEPARTMENT</Typography>
          </a>
          <nav>
            <a href="#a">Home</a>
            <a href="#a">About Us</a>
            <a href="#a">Contact Us</a>
            <a href="#a">
              <ButtonStyled variant="contained">SIGN IN</ButtonStyled>
            </a>
          </nav>
        </HeaderInner>
      </Header>

      <main>
        <FirstSection>
          <MainContainer>
            <Box>
            <Typography variant="h1">
                BSIT <br />
                <span style={{ color: '#D9AA4D' }}>Parsers</span>
            </Typography>
              <Typography variant="p" sx={{ marginTop: '15px'}}>
              An Information Technology department of Cebu Technological University - Argao Campus, aiming to produce future innovators who will lead the technological world.
              </Typography>
              <ButtonStyled className='learnMore' sx={{ marginTop: '25px', color: '#D9AA4D'}}>Learn More</ButtonStyled>
            </Box>
            <img src={parser} style={{height: '450px'}} alt="A woman and a man talking in an office" />
            <FirstSectionIcon
              src="https://prodigitas.com/wp-content/uploads/2020/03/noun_rectangle_2731804@2x.png"
              alt=""
            />
          </MainContainer>
        </FirstSection>
      </main>
    </div>
  );
}

export default LandingPage;
