import React, { useState, useEffect } from 'react';
import './styles/error.css';
import somethingWentWrong from './images/error.png';

const ErrorNotFound = () => {
  const [retryTimer, setRetryTimer] = useState(5);

  useEffect(() => {
    let interval;
    if (retryTimer > 0) {
      interval = setInterval(() => {
        setRetryTimer((prevTimer) => prevTimer - 1);
      }, 1000);
    }

    return () => clearInterval(interval);
  }, [retryTimer]);

  const handleTryAgain = () => {
    setRetryTimer(5); 
  };

  return (
    <div className="container-text">
      <img className="image" src={somethingWentWrong} alt="something-went-wrong" />
      <p className="big-text">Oops!</p>
      <p className="child-text">Wrong email or password :(</p>
      <button className="try-again" disabled={retryTimer > 0} onClick={handleTryAgain}>
        Try again {retryTimer > 0 && `in ${retryTimer}`}
      </button>
    </div>
  );
};

export default ErrorNotFound;