// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBfWei7g2kM-ZN4CJ0lYuAW7SN8DSjULZg",
  authDomain: "siamidterms.firebaseapp.com",
  projectId: "siamidterms",
  storageBucket: "siamidterms.appspot.com",
  messagingSenderId: "1041908765452",
  appId: "1:1041908765452:web:7fb7cffcee31ee53202f29",
  measurementId: "G-R2MRRXZ50Z"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);

