import React from 'react';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import Toolbar from '@mui/material/Toolbar';

const theme = createTheme();

const blogPosts = [
  {
    title: 'Bachelor of Science in Information Technology',
    content:
      'Material-UI is a popular React UI framework that provides a set of high-quality React components.',
    imageUrl: 'https://www.westcliff.edu/wp-content/uploads/2020/04/COTE-Banner-optimized-WC2.jpg',
  },
  {
    title: 'Faculty Excellence: Mentors Shaping the Future',
    content:
      'Material-UI follows the principles of Google\'s Material Design, offering a sleek and modern user interface.',
    imageUrl: 'https://scontent.fceb1-3.fna.fbcdn.net/v/t39.30808-6/314957621_498764598963373_5208328158093718381_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=5f2048&_nc_eui2=AeFXBKW_pXdZ9-gPyTuz8YsIcGoAnlMcDZxwagCeUxwNnLngMzV6v9z9mRGwqDkepem0MrCiDZzwP-FbV4GaWEqG&_nc_ohc=pcLBpmHwu5cAX9j-WeK&_nc_oc=AQmrx8gaUHukSJFi8VG5oQbRcsT3thEV1hEbgiyoyohOYjCiLQx2kS-Am4f6ih_d1vo&_nc_ht=scontent.fceb1-3.fna&oh=00_AfDpmcje1g2yuwKhTCcNipwS8_ZcZwZK-Y_ehQUi1zEwAw&oe=6557B2E8',
  },
  {
    title: 'Building Beautiful Apps',
    content:
      'With Material-UI, developers can create visually stunning and responsive web applications.',
    imageUrl: 'https://scontent.fceb1-3.fna.fbcdn.net/v/t39.30808-6/316690560_510022754504224_5201751705895437790_n.jpg?stp=c342.0.1365.1365a_dst-jpg_s851x315&_nc_cat=103&ccb=1-7&_nc_sid=5f2048&_nc_eui2=AeG8bJ1DcSH-hiGoZRAlBWNG8k7gB-5zbzfyTuAH7nNvN6KwNEnRbd2ycJcw8lEiQjRYVtj3vXlul6e1gLoJTTzv&_nc_ohc=pQ2vM0G6OI8AX-kHWop&_nc_ht=scontent.fceb1-3.fna&oh=00_AfB3NViW6RCtqMydDPn85OP5Y9n4b1sslTwTInXDvk1Gdg&oe=65580ECE',
  },
];

const Header = (props) => {
  const { title } = props;

  return (
    <React.Fragment>
      <Toolbar sx={{ backgroundColor: '#FFE3BB'}}>
        <Button size="small">Back</Button>
        <Typography
          component="h2"
          variant="h5"
          color="inherit"
          align="center"
       
          noWrap
          sx={{ flex: 1 }}
        >
          {title}
        </Typography>
        <IconButton>
          <SearchIcon />
        </IconButton>
        <Button variant="outlined" size="small" style={{}}>
          Log out
        </Button>
      </Toolbar>
    </React.Fragment>
  );
};

// ... (previous imports)

// ... (previous imports)

const Blog = () => {
  const handleLogout = () => {
    console.log('Logout clicked');
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <div
        style={{
          // backgroundImage: 'url("https://source.unsplash.com/1920x1080/?background")',
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          height: '100vh',
          display: 'flex',
          flexDirection: 'column',
          backgroundColor: 'white',
        }}
      >
        <AppBar position="static" color="transparent">
          <Header title="Congratulations!" color="white" />
          <div style={{ flexGrow: 1 }} />
        </AppBar>
        <Container
          maxWidth="lg"
          style={{
            marginTop: '20px',
            height: 'calc(100vh - 64px)', // Adjust the height as needed
            paddingTop: '20px',
            paddingBottom: '20px', // Adjust the top and bottom padding as needed
          }}
        >
          <Card
            style={{
              backgroundImage: `url(${blogPosts[0].imageUrl})`,
              backgroundSize: 'cover',
              color: 'white',
              minHeight: '400px',
              paddingTop: '20px',
              fontWeight:'700px',
              fontSize:'30px'
            }}
          >
            <CardContent>
              <Typography variant="h5" component="div">
                {blogPosts[0].title}
              </Typography>
              <Typography variant="body2" color="textSecondary">
                {blogPosts[0].content}
              </Typography>
            </CardContent>
          </Card>
          <Grid container spacing={4} paddingTop='20px'>
            {blogPosts.slice(1).map((post, index) => (
              <Grid item key={index} xs={12} md={6}>
                <Card
                  style={{
                    backgroundImage: `url(${post.imageUrl})`,
                    backgroundSize: 'cover',
                    color: 'white',
                    minHeight: '200px',
                    fontSize:'20px',
                    fontWeight:'bolder'
                  }}
                >
                  <CardContent>
                    <Typography variant="h5" component="div">
                      {post.title}
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                      {post.content}
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
            ))}
          </Grid>

          {/* Additional Blog Post */}
          <div style={{ display: 'flex', marginTop: '50px' }}>
            <Card style={{ flex: 1, marginRight: '20px',height:'200px',backgroundColor:'whitesmoke'}}>
              <CardContent>
                <Typography variant="h6" component="div" style={{ color: 'black' }}>
                Shaping Future Innovators
                </Typography>
                <Typography variant="body2" color="textSecondary" style={{ color: 'black', fontSize: '16px', fontFamily: 'Segoe UI' }}>
                Bachelor of Science in Information Technology department is not just an educational institution; it is a gateway to a world of possibilities. Through a robust curriculum, dedicated faculty,
                
                 and a vibrant community, the BSIT department empowers students to become the innovators, problem-solvers, and leaders of tomorrow's tech-driven world. 
                </Typography>
              </CardContent>
            </Card>
            <div style={{ flex: 2 }}>
              <Typography variant="h5" component="div" style={{ color: 'black', marginBottom: '20px' }}>
              Exploring the World of Information Technology: A Glimpse into the BSIT Department
              </Typography>
              <Typography variant="body2" color="textSecondary" style={{ color: 'black', fontSize: '15px', fontFamily: 'Segoe UI' }}>
           
              In the dynamic landscape of technology, where innovation is the driving force, the Bachelor of Science in Information Technology (BSIT) department stands as a beacon of knowledge, shaping the future of tech enthusiasts. This department serves as a hub for aspiring IT professionals, offering a comprehensive curriculum 
              that goes beyond the realms of coding and delves into the heart of information systems, cybersecurity, and cutting-edge technologies.   
                         </Typography>
            </div>
          </div>
        </Container>
      </div>
    </ThemeProvider>
  );
};

export default Blog;
