import logo from './logo.svg';
import {  Routes, Route } from 'react-router-dom';
import SignInSide from './components/SignInSide';
import SignUp from './components/SignUp';

function App() {
  return (
    <Routes>
        <Route path='/' element={<SignInSide />}></Route>
        <Route path='/signup' element={<SignUp />} />
    </Routes>  
  );
}

export default App;
